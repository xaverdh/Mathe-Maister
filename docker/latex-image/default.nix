{ pkgs ? import <nixpkgs> {} }: with pkgs;
dockerTools.buildImage {
  name = "latex-svg";
  contents = [
    inkscape
    texlive.combined.scheme-full
    bash
    findutils
    coreutils
    gawk
    gnused
  ];
  config = {
    WorkingDir = "/data";
    Volumes = {
      "/data" = {};
    };
  };
}
