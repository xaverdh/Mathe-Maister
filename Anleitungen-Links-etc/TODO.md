# General TODOs

 - Website?

## README etc
 - guest book

## CSV to PDF Skript
 + show date of last edit
 + show name of document, i.e. the lecture name

## CSV to APKG Skript
 + create different decks according to the priority field, creating 3 decks of different sizes
 + create every deck with and without pictures for low file size versions
 + create every deck with or without the 'text below operators' notation
 + change options, including leech limit and limit of daily repetitions and new cards

#### Less Important CSV to APKG Todos:
 + "Created" in Anki currently showing Unix 0 (i.e. 1970-01-01) (probably never gonna be fixed)
 + create a version that bundles all decks in one file for quick importing (useful at all?)
 + import every deck with the same note type, without random strings at the end, so the user can change the layout of all decks at once (needed at all?)

## Other
nothing